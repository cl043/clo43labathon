#include<stdio.h>

void input(int *a)
{
    printf("Enter the integer: ");
    scanf("%d",&*a);
}

int compute(int a, int b, int c)
{
    if(a>b)
    {
        if(a>c)
        {
            return a;
        }
        else
        return c;
    }
    else
    {
        if(b>c)
        {
            return b;
        }
        else
        return c;
    }
}

void output(int a,int b,int c,int d)
{
    printf("\nThe largest integer among %d,%d,%d is %d\n",a,b,c,d);
}

void main()
{
    int a,b,c,d;
    input(&a);
    input(&b);
    input(&c);
    d=compute(a,b,c);
    output(a,b,c,d);
}


