#include<stdio.h>

void input(int *p, float *t, float *r)
{
    printf("Enter principal value: ");
    scanf("%d",&*p);
    printf("Enter the time period in years: ");
    scanf("%f",&*t);
    printf("Enter rate of interest: ");
    scanf("%f",&*r);
}

float compute(int p, float t, float r)
{
    float s;
    s=(p*t*r)/100;
    return s;
}

void display(int p, float t, float r,float s)
{
    printf("\nFor the given\nPrinciple value: %d\nTime: %.2f\nRate of interest: %.2f\nThe value of simple interest is %.2f",p,t,r,s);
}

int main()
{
    float s,t,r;
    int p;
    input(&p,&t,&r);
    s=compute(p,t,r);
    display(p,t,r,s);
    return 0;
}