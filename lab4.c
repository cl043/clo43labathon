 #include<stdio.h>

void input(int *n)
{
  printf("Enter a number: ");
  scanf("%d",&*n);
}

void compute(int n, int arr[])
{
    int i;
    for(i=1;i<=100;i++)
    {
        arr[i]=n*i;
    }
}

void display(int n, int arr[])
{
    int i;
    printf("The multiples of %d are:\n",n);
    for(i=1;i<=100;i++)
    {
        printf("%d*%d=%d\n",n,i,arr[i]);
    }
}

void main()
{
	int n,arr[100];
	input(&n);
	compute(n,arr);
	display(n,arr);
}

