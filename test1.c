#include<stdio.h>
#include<math.h>

struct point
{
	float x;
	float y;
};

typedef struct point Point;

Point input()
{ 
	Point p;
	printf("Enter the abscissa\n");
	scanf("%f",&p.x);
	printf("Enter the ordinate\n");
	scanf("%f",&p.y);
	return p;
}

float compute(Point p1, Point p2)
{
	float distance;
	distance=sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
	return distance;
}

void output(Point p1, Point p2,float distance)
{
	printf("The distance between (%.2f,%.2f) and (%.2f,%.2f) is %.2f\n",p1.x,p1.y,p2.x,p2.y,distance);
}

int main ()
{
	Point p1,p2;
	float dist;
	p1=input();
	p2=input();
	dist=compute(p1,p2);
	output(p1,p2,dist);
	return 0;
}





