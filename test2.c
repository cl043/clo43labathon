#include<stdio.h>

struct time
{
    int h;
    int m;
};

typedef struct time Time;
 
Time input()
{
    Time a;
    printf("\nEnter hours:");
    scanf("%d",&a.h);
    printf("Enter minutes:");
    scanf("%d",&a.m);
    return a;
}

int compute (Time a)
{
    return (a.h*60)+a.m;
}
void output(Time a,int t)
{
	printf("\n%d Hours and %d minutes is equal to %d minutes\n",a.h,a.m,t);
}

void main()
{
	Time a;
	int t;
	a=input();
	t=compute(a);
	output(a,t);
}
