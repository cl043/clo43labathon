#include<stdio.h>
#include<math.h>

struct roots
{
    float real;
    float img;
};

void input(int *a, int *b, int *c)
{
    printf ("Enter the coordinate of x^2: ");
    scanf ("%d", &*a);
    printf ("Enter the coordinate of x: ");
    scanf ("%d", &*b);
    printf ("Enter the constant: ");
    scanf ("%d", &*c);
}

void compute (int a, int b, int c,struct roots *r)
{
    r->real=(float)(-b)/(2*a);
    if((b*b)<(4*a*c))
    {
        r->img=sqrt((4*a*c)-(b*b))/(2*a);
    }
    else
    {
        r->img=sqrt((b*b)-(4*a*c))/(2*a);
    }
}

void display(int a, int b, int c, struct roots r)
{
   if(((b)*(b))<(4*a*c))
   {
       printf("\nThe Roots Are Imaginary\n");
       printf("The Roots of (%d)(x^2)+(%d)(x)+(%d) Are (%.2f)+(%.2f)i And (%.2f)-(%.2f)i\n",a,b,c,r.real,r.img,r.real,r.img);
   }
   else
   {
    printf("\nThe Roots Are Real\nThe Roots Of (%d)(x^2)+(%d)(x)+(%d) Are %.2f And %.2f\n",a,b,c,(r.real+r.img),(r.real-r.img)); 
   }
}

int main()
{
    int a,b,c;
    input(&a,&b,&c);
    struct roots r;
    compute(a,b,c,&r);
    display(a,b,c,r);
    return 0;
}
