#include<stdio.h>

float input()
{
    float r;
    printf("\nEnter the radius of the circle: ");
    scanf("%f",&r);
    return r;
}

float compute(float r)
{
    float area;
    area=3.14*r*r;
    return area;
}

void output(float r,float area)
{
    printf("\nArea of circle of radius %0.2f is %0.2f\n",r,area);
}

void main()
{
    float r,area;
    r=input();
    area=compute(r);
    output(r,area);
}